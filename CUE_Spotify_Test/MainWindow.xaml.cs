﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Collections.Specialized;
using System.Reflection;
using System.Diagnostics;
using System.Drawing;
using System.Timers;
using System.Web;
using System.Net;
using System.Net.Http;
using System.IO;
using Microsoft.Win32;
using Newtonsoft.Json;
using CUE.NET;
using CUE.NET.Exceptions;
using CUE.NET.Devices.Keyboard;
using CUE.NET.Devices.Generic;
using CUE.NET.Devices.Mouse;
using CUE.NET.Devices.Mousemat;
using CUE.NET.Devices.Headset;
using CUE.NET.Devices.Keyboard.Enums;

namespace CUE_Spotify_Test {

    public partial class MainWindow : Window {

        static string CLIENT_ID = "b619f21779924f95b400ee98ef9a529f";
        static string CLIENT_SECRET = "287420073b68496f93a3586a39a620c1";
        static string REDIRECT_URI = "test://spotifycallback";
        static string BASE_URL_AUTH = "https://accounts.spotify.com/authorize?";
        static string SCOPE = "user-read-private user-read-email user-read-playback-state";
        static string STATE = "CHANGE_THIS_LATER";

        static string ACCESS_TOKEN = "";
        static string REFRESH_TOKEN = "";

        public string CURRENT_HREF = "";


        public MainWindow() {
            InitializeComponent();

            InitCue();

            loginBrowser.Navigate(LoginUrl());
        }

        string LoginUrl() {
            return string.Format("{0}response_type=code&client_id={1}&scope={2}&redirect_uri={3}&state={4}", BASE_URL_AUTH, CLIENT_ID, SCOPE, REDIRECT_URI, STATE);
        }

        private static void ShowDebugDialog(string body) {
            MessageBox.Show(body);
        }

        private async void SendCodePost(string code) {
            loginBrowser.Visibility = Visibility.Collapsed;
            HttpClientHandler handler = new HttpClientHandler { UseDefaultCredentials = true };
            StringContent sc = new StringContent("code=" + code + "&redirect_uri=" + REDIRECT_URI + "&grant_type=authorization_code");
            using (var client = new HttpClient(handler)) {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(string.Format("{0}:{1}", CLIENT_ID, CLIENT_SECRET))));
                sc.Headers.ContentType.MediaType = "application/x-www-form-urlencoded";
                try {
                    HttpResponseMessage resp = await client.PostAsync("https://accounts.spotify.com/api/token", sc);
                    if (resp.IsSuccessStatusCode) {
                        string json = await resp.Content.ReadAsStringAsync();
                        AuthorizationObject obj = JsonConvert.DeserializeObject<AuthorizationObject>(json);
                        ACCESS_TOKEN = obj.access_token;
                        REFRESH_TOKEN = obj.refresh_token;
                        System.Timers.Timer t = new System.Timers.Timer(obj.expires_in);
                        t.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                        t.Enabled = true;
                    } else {
                        ShowDebugDialog(resp.StatusCode.ToString());
                    }
                } catch (Exception e) {
                    ShowDebugDialog(e.Message);
                }
            }

            FilterApiCall("/v1/me/player/currently-playing");
        }

        private void FilterApiCall(string param) {
            switch(param) {
                case "/v1/me/player/currently-playing":
                    MakeCurrentlyPlayingCall();
                    break;
                default:
                    return;
            }

            System.Timers.Timer t = new System.Timers.Timer(100);
            t.Elapsed += new ElapsedEventHandler(OnSongChanged);
            t.Enabled = true;
        }

        private async void MakeCurrentlyPlayingCall() {
            string url = "https://api.spotify.com/v1/me/player/currently-playing";
            HttpClientHandler handler = new HttpClientHandler { UseDefaultCredentials = true };
            using (var client = new HttpClient(handler)) {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Authorization", string.Format("{0} {1}", "Bearer", ACCESS_TOKEN));
                try {
                    HttpResponseMessage msg = await client.GetAsync(url);
                    if (msg.IsSuccessStatusCode) {
                        string json = await msg.Content.ReadAsStringAsync();
                        CurrentlyPlayingObject obj = JsonConvert.DeserializeObject<CurrentlyPlayingObject>(json);
                        if (obj.item.href != CURRENT_HREF) {
                            CURRENT_HREF = obj.item.href;
                            GetColorReferences(obj.item.album.images[0].url, obj.item.album.images[0].height, obj.item.album.images[0].width);
                        }
                    } else {
                        ShowDebugDialog(msg.StatusCode.ToString());
                    }
                } catch (Exception e) {
                    ShowDebugDialog(e.Message);
                }
            }
        }

        private static async void RefreshToken() {
            HttpClientHandler handler = new HttpClientHandler { UseDefaultCredentials = true };
            StringContent sc = new StringContent("grant_type=refresh_token&refresh_token=" + REFRESH_TOKEN);
            using (var client = new HttpClient(handler)) {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(string.Format("{0}:{1}", CLIENT_ID, CLIENT_SECRET))));
                sc.Headers.ContentType.MediaType = "application/x-www-form-urlencoded";
                try {
                    HttpResponseMessage resp = await client.PostAsync("https://accounts.spotify.com/api/token", sc);
                    if (resp.IsSuccessStatusCode) {
                        string json = await resp.Content.ReadAsStringAsync();
                        AuthorizationObject obj = JsonConvert.DeserializeObject<AuthorizationObject>(json);
                        ACCESS_TOKEN = obj.access_token;
                        REFRESH_TOKEN = obj.refresh_token;
                        System.Timers.Timer t = new System.Timers.Timer(obj.expires_in);
                        t.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                        t.Enabled = true;
                    } else {

                    }
                } catch (Exception e) {
                    Console.WriteLine("{0}", e.Message);
                }
            }
        }

        private void GetColorReferences(string imageUrl, int h, int w) {
            Height = h;
            Width = w;
            WebClient wc = new WebClient();
            byte[] imageBytes = wc.DownloadData(imageUrl);
            MemoryStream memStream = new MemoryStream(imageBytes);
            Image img = Image.FromStream(memStream);
            memStream.Close();
            Bitmap bmp = new Bitmap(img);
            BitmapImage bmpImage;
            using (var ms = new MemoryStream()) {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ms.Position = 0;

                var bi = new BitmapImage();
                bi.BeginInit();
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.StreamSource = ms;
                bi.EndInit();
                BitmapPalette palette = new BitmapPalette(bi, 5);
                ShowLights(palette);
                bmpImage = bi;
                Application.Current.Dispatcher.Invoke(() => {
                    albumArt.Source = bi;
                });
            }
        }

        private void InitCue() {
            try {
                CueSDK.Initialize(true);
            } catch (WrapperException e) {
                ShowDebugDialog(e.Message);
            }
        }

        private void ShowLights(BitmapPalette bmp) {
            CorsairKeyboard keeb = CueSDK.KeyboardSDK;
            if (keeb != null) {
                foreach (CorsairLed ledId in keeb.GetLeds()) {
                    ledId.Color = RandomColor(bmp);
                    keeb.Update();
                }
            }
            CorsairMouse mouse = CueSDK.MouseSDK;
            if (mouse != null) {
                foreach (CorsairLed ledId in mouse.GetLeds()) {
                    ledId.Color = RandomColor(bmp);
                    mouse.Update();
                }
            }
            CorsairHeadset head = CueSDK.HeadsetSDK;
            if (head != null) {
                foreach (CorsairLed ledId in head.GetLeds()) {
                    ledId.Color = RandomColor(bmp);
                    head.Update();
                }
            }
            CorsairMousemat mm = CueSDK.MousematSDK;
            if (mm != null) {
                foreach (CorsairLed ledId in mm.GetLeds()) {
                    ledId.Color = RandomColor(bmp);
                    mm.Update();
                }
            }
        }

        private Color RandomColor(BitmapPalette bmp) {
            Random r = new Random();
            int rand = r.Next(0, bmp.Colors.ToArray().Length);
            Thread.Sleep(10);
            return Color.FromArgb(bmp.Colors[rand].A, bmp.Colors[rand].R, bmp.Colors[rand].G, bmp.Colors[rand].B);
        }

        private static void OnTimedEvent(object src, ElapsedEventArgs e) {
            RefreshToken();
        }

        private void OnSongChanged(object src, ElapsedEventArgs e) {
            Application.Current.Dispatcher.Invoke(() => {
                MakeCurrentlyPlayingCall();
            });
        } 

        private void loginBrowser_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e) {
            string uriString = e.Uri.Scheme;
            if (uriString == "test") {
                string code, state;
                state = HttpUtility.ParseQueryString(e.Uri.Query).Get("state");
                if (state == STATE) {
                    code = HttpUtility.ParseQueryString(e.Uri.Query).Get("code");
                    SendCodePost(code);
                }
            }
        }
    }

    public class AuthorizationObject {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string scope { get; set; }
    }

    public class CurrentlyPlayingObject {
        public DeviceObject device { get; set; }
        public string repeat_state { get; set; }
        public bool shuffle_state { get; set; }
        public ContextObject context { get; set; }
        public long timestamp { get; set; }
        public int progress_ms { get; set; }
        public bool is_playing { get; set; }
        public FullTrackObject item { get; set; }
    }

    public class DeviceObject {
        public string id { get; set; }
        public bool is_active { get; set; }
        public bool is_restricted { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int volume_percent { get; set; }
    }

    public class ContextObject {
        public string uri { get; set; }
        public string href { get; set; }
        public ExternalUrlObject external_urls { get; set; }
        public string type { get; set; }
    }

    public class ExternalUrlObject {
        public string[] key { get; set; }
        public string[] value { get; set; }
    }

    public class ExternalIdObject {
        public string[] key { get; set; }
        public string[] value { get; set; }
    }

    public class ImageObject {
        public int height { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }
    
    public class SimplifiedArtistObject {
        public ExternalUrlObject external_urls { get; set; }
        public string href { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string uri { get; set; }
    }

    public class SimplifiedAlbumObject {
        public string album_type { get; set; }
        public SimplifiedArtistObject[] artists { get; set; }
        public string[] available_markets { get; set; }
        public ExternalUrlObject external_urls { get; set; }
        public string href { get; set; }
        public string id { get; set; }
        public ImageObject[] images { get; set; }
        public string name { get; set; }
        public string uri { get; set; }
    }

    public class TrackLinkObject {
        public ExternalUrlObject external_urls { get; set; }
        public string href { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public string uri { get; set; }
    }

    public class FullTrackObject {
        public SimplifiedAlbumObject album { get; set; }
        public SimplifiedArtistObject[] artists { get; set; }
        public string[] available_markets { get; set; }
        public long disc_number { get; set; }
        public long duration_ms { get; set; }
        public Boolean @explicit { get; set; }
        public ExternalIdObject external_ids { get; set; }
        public string href { get; set; }
        public string id { get; set; }
        public bool is_playable { get; set; }
        public TrackLinkObject linked_from { get; set; }
        public string name { get; set; }
        public long popularity { get; set; }
        public string preview_url { get; set; }
        public int track_number { get; set; }
        public string type { get; set; }
        public string uri { get; set; }
    }

    public class DeviceDisplayObject {
        public DeviceObject[] devices { get; set; }
    }
}
